import Vue from 'vue';
import Vuex from 'vuex';
import state from './store/state';
import getters from './store/getters';
import mutations from './store/mutations';
import actions from './store/actions';

Vue.use(Vuex);

// create store
/* eslint-disable no-new */
const store = new Vuex.Store({
  // default state
  state,
  // getter methods (to access computed values from state in components)
  getters,
  // mutations must be synchronous
  mutations,
  // actions can be asynchronous
  actions,
});

export default store;
