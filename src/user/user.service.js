import authHeader from './../_helpers/auth-header';

function logout() {
  // remove user from local storage to log user out
  localStorage.removeItem('user');
  localStorage.removeItem('token');
}

// Verify server response
function handleResponse(response) {
  return response.text().then((text) => {
    if (!response.ok) {
      if (response.status === 401) {
        // auto logout if 401 response returned from api
        logout();
        return Promise.reject(401);
      } else if (response.status === 404) {
        // auto logout if 404 response returned from api
        logout();
        return Promise.reject(404);
      } else if (response.status === 409) {
        // auto logout if 409 response returned from api
        logout();
        return Promise.reject(409);
      }

      const data = text && JSON.parse(text);
      const error = (data && data.message) || response.statusText;
      return Promise.reject(error);
    }

    const data = text && JSON.parse(text);
    return data;
  });
}

// Login user
function login(email, password) {
  const requestOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ email, password }),
  };
  return fetch('http://vps631308.ovh.net/auth/login', requestOptions)
    .then(handleResponse)
    .then((user) => {
      let updatedUser;
      // login successful if there's a user in the response
      if (user) {
        // store user details and basic auth credentials in local storage
        // to keep user logged in between page refreshes
        updatedUser = {
          ...user,
          authdata: window.btoa(`${email} : ${password}`),
        };
        localStorage.setItem('user', JSON.stringify(user));
      }
      return updatedUser;
    });
}

// registration, creation account
function registration(email, username, password) {
  const requestOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ email, username, password }),
  };

  return fetch('http://vps631308.ovh.net/auth/register', requestOptions)
    .then(handleResponse)
    .then((user) => {
      let updatedUser;
      // login successful if there's a user in the response
      if (user) {
        // store user details and basic auth credentials in local storage
        // to keep user logged in between page refreshes
        updatedUser = {
          ...user,
          authdata: window.btoa(`${email} : ${password}`),
        };
      }
      return updatedUser;
    });
}

function getAll() {
  const requestOptions = {
    method: 'GET',
    headers: authHeader(),
  };

  return fetch('http://vps631308.ovh.net/auth/users', requestOptions).then(handleResponse);
}

export default {
  login,
  logout,
  getAll,
  registration,
};
