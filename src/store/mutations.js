/*
  mutations from the store
  mutations change the store synchronously
*/

// mutations must be synchronous
const mutations = {

  // category related mutations
  /*
    change current page number (pagination)
  */
  setPageNumber(state, number) {
    state.currentPage = number;
  },
  /*
    toggle category selection on click on category name
  */
  toggleSelectedCategoryName(state, categoryName) {
    // remove if existing
    if (state.selectedCategoryNames.indexOf(categoryName) !== -1) {
      state.selectedCategoryNames = state.selectedCategoryNames
        .filter(el => el !== categoryName);
    // else add category
    } else {
      state.selectedCategoryNames = state.selectedCategoryNames.concat([categoryName]);
    }

    state.currentPage = 1;
  },

  // form related mutations
  /*
    update form message with given message
  */
  updateFormMessage(state, message) {
    state.formMessage = message;
  },

  // product related mutations
  /*
    update a product quantity
    values: {
      product: {
        id: int
      }
      quantity: int
    }
  */
  updateQuantity(state, values) {
    const updatedList = state.foodList.map((el) => {
      // copy object
      const newFoodItem = {
        ...el,
        stock: el.stockId,
      };

      if (el.product.id === values.product.id) {
        newFoodItem.quantity = values.quantity;
      }
      return newFoodItem;
    });
    const filterdNoQuantityList = updatedList.filter(el => el.quantity > 0);

    state.foodList = filterdNoQuantityList;
  },
  /*
    remove product from product list
    product: {
      id: int
    }
  */
  removeProduct(state, product) {
    if (product) {
      state.foodList = state.foodList.filter(el => el.product.id !== product.id);
    }
  },
  /*
    set modification flag for product with given id
  */
  setModified(state, productId) {
    state.foodList = state.foodList.map((product) => {
      if (product.id === productId) {
        const updatedProduct = { ...product };
        updatedProduct.isModified = true;
        return updatedProduct;
      }
      return product;
    });
  },
  /*
    add item to product list
  */
  addItemToFoodList(state, item) {
    state.foodList = state.foodList.concat([item]);
    state.currentProductProposal = null;
  },
  /*
    update previewed item from barcode
  */
  updateBarcodeProposition(state, productProposal) {
    state.currentProductProposal = productProposal;
  },
  /*
    update product list
  */
  updateProductList(state, productList) {
    if (productList === null) {
      state.foodList = [];
    } else {
      state.foodList = productList;
    }
  },
  /*
    set selected product from given id
  */
  setSelectedProduct(state, productId) {
    const selectedProduct = state.foodList.find(el => el.product.id === parseInt(productId, 10));
    state.selectedProduct = selectedProduct;
  },
  /*
    show product detail card flag
  */
  showProductDetail(state, shouldDisplay) {
    state.showProductDetail = shouldDisplay;
  },

  // freegos related mutations
  /*
    update freegos list
  */
  updateFreegos(state, freegos) {
    state.stocks = freegos;
  },
  /*
    add a freego to the list
  */
  addFreego(state, freego) {
    state.stocks = state.stocks.concat([freego]);
  },
  /*
    remove freego from the list
  */
  removeFreego(state, freego) {
    state.stocks = state.stocks.filter(el => el.id !== freego.id);
  },
  /*
    update the list of user invited to currently selected freego
  */
  updateInvitedUserList(state, invitedUserList) {
    state.invitedUserList = invitedUserList;
  },
  /*
    remove user from invited list for current freego
  */
  uninviteUser(state, userId) {
    state.invitedUserList = state.invitedUserList.filter((el) => {
      const match = el.id !== userId;
      return match;
    });
  },

  // authentication related mutations
  /*
    update id for current user given the token (base 64 decode)
  */
  updateCurrentUserId(state, accessToken) {
    if (accessToken) {
      const tokenArray = accessToken.split('.');
      if (tokenArray.length > 1) {
        const decodedToken = JSON.parse(atob(tokenArray[1]));
        if (decodedToken) {
          state.currentUserId = decodedToken.identity;
        }
      }
    } else {
      state.currentUserId = '';
    }
  },
  /*
    set selected freego from given id
  */
  setSelectedFreego(state, freegoId) {
    state.selectedFreegoId = freegoId;
  },
  /*
    is token set
  */
  isTokenSet(state, isSet) {
    state.isTokenSet = isSet;
    if (!isSet) {
      state.currentUserId = '';
    }
  },
};

export default mutations;
