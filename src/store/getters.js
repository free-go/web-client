/*
  getters from the store
  to access computed values from the store
*/

const getters = {
  /*
    filter list of products (filterd by category)
  */
  filteredFoodList(state) {
    const hasFilter = state.selectedCategoryNames.length > 0;
    const filteredFoodList = hasFilter ? (
      state.foodList.filter(el => state.selectedCategoryNames.indexOf(el.category) !== -1)
    ) : state.foodList;

    return filteredFoodList;
  },
  /*
    get freego name form id
    composed getter (returns function)
  */
  getFreegoName(state) {
    return (freeGoId) => {
      const parsedId = Number.parseInt(freeGoId, 10);
      const stockIndex = state.stocks.findIndex(el => el.id === parsedId);

      if (stockIndex !== -1) {
        return state.stocks[stockIndex].name;
      }
      return null;
    };
  },
  /*
    define if current user is the owner of current freego
  */
  isOwner(state) {
    return (freegoId, userId) => {
      const freego = state.stocks.find((el) => {
        const match = parseInt(el.id, 10) === parseInt(freegoId, 10);
        return match;
      });
      if (freego) {
        return (parseInt(freego.owner, 10) === parseInt(userId, 10));
      }
      return false;
    };
  },
};

export default getters;
