/*
  actions for the store
  these can be asynchronous and return promises
*/

import axios from 'axios';

axios.defaults.headers.common.Authorization = `Bearer ${localStorage.getItem('token')}`;

// base url for API calls
const BASE_URL = 'http://vps631308.ovh.net';

// actions can be asynchronous
const actions = {
  /*
    set user token to be included in every axios call
  */
  updateUserToken() {
    axios.defaults.headers.common.Authorization = `
      Bearer ${localStorage.getItem('token')}
    `;
  },

  // freegos related actions
  /*
    retrieve all freego for user
  */
  getAllFreegos({ commit }) {
    axios
      .get(`${BASE_URL}/stock`)
      .then((response) => {
        commit('updateFreegos', response.data);
      });
  },
  /*
    retieve single freego
  */
  getFreego({ commit }, freegoId) {
    axios
      .get(`${BASE_URL}/stock/${freegoId}`)
      .then((response) => {
        commit('updateFreego', response.data);
      });
  },
  /*
    add freego to users freegos
  */
  addFreego({ commit }, freego) {
    axios
      .post(`${BASE_URL}/stock`, {
        name: freego.name,
      })
      .then((response) => {
        if (response.status !== 201) {
          commit('updateFormMessage', 'Erreur : erreur lors de la création');
        } else {
          commit('addFreego', response.data);
          commit('updateFormMessage', 'Succès : Freego ajouté');
        }
      });
  },
  /*
    remove freego from users freego
  */
  removeFreego({ commit }, freegoId) {
    axios
      .delete(`${BASE_URL}/stock/${freegoId}`)
      .then((response) => {
        if (response.status === 200) {
          commit('removeFreego', response.data);
        }
      });
  },

  // products related actions
  /*
    get all products for a freego
  */
  getAllProducts({ commit }, stockId) {
    axios
      .get(`${BASE_URL}/product/stock/${stockId}`)
      .then((response) => {
        if (response.status === 404) {
          commit('updateProductList', null);
        }
        commit('updateProductList', response.data);
      })
      .catch(() => {
        commit('updateProductList', null);
      });
  },
  /*
    update a product quantity in a freego
    or remove it if quantity is zero
    payload: {
      item: {
        stock: int,
        product: {
          id: int
        }
        amout: int
      }
    }
  */
  updateProductQuantity({ commit }, payload) {
    const stockId = payload.item.stock;
    const productId = payload.item.product.id;
    if (payload.amount > 0) {
      axios
        .put(`${BASE_URL}/product/${productId}/stock/${stockId}`, {
          quantity: payload.amount,
        })
        .then((response) => {
          if (response.status === 200) {
            commit('updateQuantity', response.data);
          }
        });
    } else {
      axios
        .delete(`${BASE_URL}/product/${productId}/stock/${stockId}`)
        .then((response) => {
          if (response.status === 200) {
            commit('removeProduct', response.data);
          }
        });
    }
  },
  /*
    add a product to a freego
    payload: {
      stockId: int
      product: {
        id: int
      }
      quantity: int
    }
  */
  addProduct({ commit }, payload) {
    const foodItem = payload.product;
    if (foodItem && payload.quantity > 0) {
      axios
        .post(`${BASE_URL}/product/stock/${payload.stockId}`, {
          product_id: foodItem.id,
          quantity: payload.quantity,
        })
        .then((response) => {
          if (response.status === 201) {
            commit('addItemToFoodList', payload);
          }
        });
    } else {
      commit('updateBarcodeProposition', null);
    }
  },
  /*
    get product object for given barcode
    payload: {
      barcode: int
    }
  */
  displayProductFromBarcode({ commit }, payload) {
    const barcode = payload.barcode;
    axios
      .get(`${BASE_URL}/product/barcode/${barcode}`)
      .then((response) => {
        if (response.status === 404) {
          commit('updateBarcodeProposition', null);
        }
        commit('updateBarcodeProposition', {
          ...response.data,
          barcode,
        });
      })
      .catch(() => {
        commit('updateBarcodeProposition', null);
      });
  },

  // user related actions
  /*
    grant access to a users freego to another user
    payload: {
      userId: int
      freegoId: int
      freegoName: string
    }
  */
  addUserToFreego({ commit }, payload) {
    const userId = payload.userId;
    const freegoId = payload.freegoId;
    const freegoName = payload.freegoName;
    let message = '';

    return new Promise((resolve, reject) => {
      if (userId) {
        axios
          .post(`${BASE_URL}/stock/${freegoId}/adduser`, {
            user_id: userId,
          })
          .then((response) => {
            if (response.status === 200) {
              message = `User ${userId} successfuly added to freego ${freegoName}`;
              commit('updateFormMessage', message);
            } else {
              message = `The User ${userId} could not be added to freego ${freegoName}`;
              commit('updateFormMessage', message);
            }
            resolve();
          })
          .catch(() => {
            message = `The User ${userId} could not be added to freego ${freegoName}`;
            commit('updateFormMessage', message);
            reject();
          });
        commit('updateFormMessage', message);
        message = 'A valid user id must be set';
        commit('updateFormMessage', message);
      } else {
        reject();
      }
    });
  },
  /*
    get list of invited users
  */
  getInvitedUserList({ commit }, freegoId) {
    if (freegoId) {
      axios
        .get(`${BASE_URL}/stock/${freegoId}/users`)
        .then((response) => {
          if (response.status === 200) {
            commit('updateInvitedUserList', response.data);
          }
        });
    }
  },
  /*
    uninvite user
    payload: {
      userId: int,
      freegoId: string,
    }
  */
  uninviteUser({ commit }, payload) {
    if (payload) {
      const userId = parseInt(payload.userId, 10);
      const freegoId = parseInt(payload.freegoId, 10);
      if (userId && freegoId) {
        axios
          .delete(`${BASE_URL}/stock/${freegoId}/user/${userId}`)
          .then((response) => {
            if (response.status === 200) {
              if (response.data) {
                commit('uninviteUser', response.data.user_id);
              }
            }
          });
      }
    }
  },
};

export default actions;
