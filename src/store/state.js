/*
  state by default
*/

// default state
const state = {
  // list of selected categories (array of strings)
  selectedCategoryNames: [],
  // selected product object (object)
  selectedProduct: null,
  // select freego id (int)
  selectedFreegoId: null,
  // pagination limit (int)
  paginationLimit: 10,
  // current page navigation (int)
  currentPage: 1,
  // product list (array of objects)
  foodList: [],
  // freego list (array of objects)
  stocks: [],
  // current product preview from barcode (object)
  currentProductProposal: null,
  // form message (string)
  formMessage: '',
  // current user id (string)
  currentUserId: '',
  // show product detail card (boolean)
  showProductDetail: false,
  // list of users invited to selected freego (array of object)
  invitedUserList: [],
  // is token set
  isTokenSet: false,
};

export default state;
