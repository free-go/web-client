import Vue from 'vue';
import Router from 'vue-router';
import FreegoPage from '@/components/pages/FreegoPage';
import FreegoSettingsPage from '@/components/pages/FreegoSettingsPage';
import StocksPage from '@/components/pages/StocksPage';
import LoginPage from '@/components/pages/LoginPage';
import AddStockPage from '@/components/pages/AddStockPage';
import RegistrationPage from '@/components/pages/RegistrationPage';
import NotFoundPage from '@/components/pages/NotFoundPage';

Vue.use(Router);

const router = new Router({
  routes: [
    // login page
    {
      path: '/login',
      name: 'LoginPage',
      component: LoginPage,
    },
    // add a stock page
    {
      path: '/add',
      name: 'AddStockPage',
      component: AddStockPage,
    },
    // register a user page
    {
      path: '/register',
      name: 'RegistrationPage',
      component: RegistrationPage,
    },
    {
      // match "/" and "/stocks" to list all stocks (my freegos)
      path: '/(stocks)?',
      name: 'StocksPage',
      component: StocksPage,
    },
    // display a single freego page
    {
      path: '/freego/:id',
      name: 'FreegoPage',
      component: FreegoPage,
    },
    // settings page for a freego
    {
      path: '/freego/:id/settings',
      name: 'FreegoSettingsPage',
      component: FreegoSettingsPage,
    },
    // page not found (wildcard)
    {
      path: '*',
      name: 'NotFoundPage',
      component: NotFoundPage,
    },
  ],
});

/* check performed before each routing action */
router.beforeEach((to, from, next) => {
  // redirect to login page if not logged in and trying to access a restricted page
  const publicPages = ['/login', '/register'];
  const authRequired = !publicPages.includes(to.path);
  const loggedIn = localStorage.getItem('user');

  if (authRequired && !loggedIn) {
    return next('/login');
  }

  return next();
});

export default router;
